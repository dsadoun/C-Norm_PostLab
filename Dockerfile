# python:3.6.9 for interface mode
FROM python:3.7
#FROM tensorflow/tensorflow:2.0.0
#docker pull tensorflow/tensorflow:2.2.2

LABEL  Driss Sadoun <driss.sadoun@postlab.fr>
LABEL description="docker image C-norm interface" 

RUN pip install --upgrade pip
RUN pip install protobuf==3.20.1
RUN pip install tensorflow==2.0.0
RUN pip install pronto
#RUN pip install pronto==2.4.1
RUN pip install gensim==3.8.3
RUN pip install numpy
RUN pip install scikit-learn

COPY data /c-norm/data
COPY README.md /c-norm/README.md
COPY onto.py /c-norm/onto.py
COPY predict.py /c-norm/predict.py
COPY train.py /c-norm/train.py
COPY word2term.py /c-norm/word2term.py

COPY LICENSE /c-norm/LICENSE

COPY results /c-norm/results

COPY scripts/run.sh /c-norm/scripts/run.sh

RUN chmod +x /c-norm/scripts/run.sh

ENV COMMAND=""
ENV PARAMETERS=""

WORKDIR /c-norm

ENTRYPOINT ["sh","scripts/run.sh"]


# export COMMAND="predict.py"
# export PARAMETERS="--word-vectors-bin data/VST_count0_size100_iter50.model --ontology data/OntoBiotope_BioNLP-ST-2016.obo --terms data/terms_dev.json --inputModel data/trainedTFmodel/ --output results/output.txt"





# sudo docker build -t dsadoun/postlab:c-norm-interface .


# sudo docker run  -v /home/driss/dev/C-Norm_PostLab:/c-norm/results -e COMMAND="predict.py" -e PARAMETERS="--word-vectors-bin data/VST_count0_size100_iter50.model --ontology data/OntoBiotope_BioNLP-ST-2016.obo --terms data/terms_dev.json --inputModel data/trainedTFmodel/ --output results/output.txt" dsadoun/postlab:c-norm-interface


# sudo docker run dsadoun/postlab:c-norm-interface train.py --word-vectors-bin data/VST_count0_size100_iter50.model --ontology data/OntoBiotope_BioNLP-ST-2016.obo --terms data/terms_train.json --attributions data/attributions_train.json --outputModel data/trainedTFmodel/ --epochs 5

# sudo docker run dsadoun/postlab:c-norm-interface predict.py --word-vectors-bin data/VST_count0_size100_iter50.model --ontology data/OntoBiotope_BioNLP-ST-2016.obo --terms data/terms_dev.json --inputModel data/trainedTFmodel/ --output data/results/testResults.txt

# sudo docker run -v /home/driss/dev/C-Norm_PostLab:/c-norm/results dsadoun/postlab:c-norm-interface predict.py --word-vectors-bin data/VST_count0_size100_iter50.model --ontology data/OntoBiotope_BioNLP-ST-2016.obo --terms data/terms_dev.json --inputModel data/trainedTFmodel/ --output data/results/testResults.txt



# sudo docker push dsadoun/postlab:c-norm-interface
# sudo docker pull dsadoun/postlab:c-norm-interface