#!/bin/bash


# sh scripts/predic.sh COMMAND="src/concord-fileDB.py" PARAMETERS="200"

# export COMMAND="predict.py"
# export PARAMETERS="--word-vectors-bin data/VST_count0_size100_iter50.model --ontology data/OntoBiotope_BioNLP-ST-2016.obo --terms data/terms_dev.json --inputModel data/trainedTFmodel/ --output data/results/testResults.txt"

#echo "Execute: python3 predict.py --word-vectors-bin data/VST_count0_size100_iter50.model --ontology data/OntoBiotope_BioNLP-ST-2016.obo --terms data/terms_dev.json --inputModel data/trainedTFmodel/ --output data/results/testResults.txt
echo "Execute: python3 $COMMAND $PARAMETERS"

python3 $COMMAND $PARAMETERS